import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./pages/Home";
import Implementation from "./pages/Implementation";
import Info from "./pages/Info";
import NotFound from "./pages/NotFound";

export default function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/info" exact>
            <Info />
          </Route>
          <Route path="/implementation" exact>
            <Implementation />
          </Route>
          <Route component={NotFound} />
        </Switch>
      </div>
    </Router>
  );
}
