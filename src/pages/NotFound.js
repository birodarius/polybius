import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Container } from "react-bootstrap";

import "../assets/scss/NotFound.scss";

export default class NotFound extends Component {
  render() {
    return (
      <Container className="not-found">
        <h1>Oops! Pagina nu a fost gasită...</h1>
        <Link to="/">
          <Button color="primary" type="submit" className="mt-3">
            Înapoi la pagina principală
          </Button>
        </Link>
      </Container>
    );
  }
}
