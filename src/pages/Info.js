import React, { Component } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

import PolybiusSquare from "../components/Info/PolybiusSquare";

import "../assets/scss/Info.scss";
import BasicForm from "../components/Info/BasicForm";
import Applications from "../components/Info/Applications";
import Adaptations from "../components/Info/Adaptations";

export default class Info extends Component {
  render() {
    return (
      <div>
        <Navbar bg="light" expand="lg" className="info-navbar">
          <Navbar.Brand href="/">Polybius</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Link to="/" className="nav-link">
                Acasă
              </Link>
              <Link to="/implementation" className="nav-link">
                Implementarea algoritmului
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Container className="mt-5">
          <PolybiusSquare />
          <BasicForm />
          <Applications />
          <Adaptations />
        </Container>
      </div>
    );
  }
}
