import React, { Component } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

import NoKeyEncryption from "../components/Implementation/NoKeyEncryption";
import MyKeyEncription from "../components/Implementation/MyKeyEncription";

import "../assets/scss/Implementation.scss";
import YourKeyEncryption from "../components/Implementation/YourKeyEncription";

export default class Implementation extends Component {
  render() {
    return (
      <div className="implementation">
        <Navbar bg="light" expand="lg" className="info-navbar">
          <Navbar.Brand href="/">Polybius</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Link to="/" className="nav-link">
                Acasă
              </Link>
              <Link to="/info" className="nav-link">
                Prezentarea algoritmului
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Container>
          <h2 className="mt-5">
            Implementarea algoritmului de criptare Polybius
          </h2>
          <NoKeyEncryption />
          <hr />
          <MyKeyEncription />
          <hr />
          <YourKeyEncryption />
        </Container>
      </div>
    );
  }
}
