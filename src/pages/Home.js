import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Container, Jumbotron, Row } from "react-bootstrap";

import "../assets/scss/Home.scss";

export default class Home extends Component {
  render() {
    return (
      <Container>
        <Jumbotron className="mt-5">
          <h1 className="display-3">Alogoritmul lui Polybius</h1>
          <p className="lead">Proiect pentru cursul de Criptografie.</p>
          <hr className="my-2" />
          <p>
            Prezentarea și implementarea algoritmului de criptare/Criptosistemul
            lui Polybius.
          </p>
          <Container className="mt-3">
            <Row>
              <Link to="/info">
                <Button variant="info" size="lg" type="submit">
                  Vezi prezentarea
                </Button>
              </Link>
              <Link to="/implementation">
                <Button variant="warning" className="ml-3" size="lg">
                  Vezi implementarea
                </Button>
              </Link>
            </Row>
          </Container>
        </Jumbotron>
      </Container>
    );
  }
}
