import React, { Component } from "react";
import { Button, Col, Row, Table } from "react-bootstrap";

import { isEmpty } from "lodash";

export default class MyKeyEncription extends Component {
  constructor() {
    super();

    this.state = {
      plainText: "",
      basicMatrix: this.getMatrixFromKey(),
      encriptedText: "",
    };
  }

  setEncriptionKey = (keyPhrase) => {
    const alphabet = "abcdefghijklmnopqrstuvwxyz";
    let keyPhraseAlphabet = keyPhrase.replace(" ", "") + alphabet;
    let finalKey = [
      ...new Set(keyPhraseAlphabet.replace("j", "i").split("")),
    ].join("");

    return finalKey.toUpperCase();
  };

  getMatrixFromKey = () => {
    let matrix = new Array(5);
    let key = this.setEncriptionKey("birodarius");
    let splitKey = key.split("");
    let currentIndex = 0;

    for (let i = 0; i < 5; i++) {
      matrix[i] = new Array(5);

      for (let j = 0; j < 5; j++) {
        matrix[i][j] = splitKey[currentIndex];
        currentIndex++;
      }
    }

    return matrix;
  };

  encrypt = (textToEncrypt) => {
    const { basicMatrix } = this.state;
    let splitText = textToEncrypt.split("");
    let encriptedString = "";

    splitText.forEach((letter) => {
      for (let i = 0; i < 5; i++) {
        let letterIndex = basicMatrix[i].indexOf(letter.toUpperCase());

        if (letterIndex !== -1) {
          encriptedString = encriptedString + `${i + 1}${letterIndex + 1} `;
        }
      }
    });

    this.setState({ encriptedText: encriptedString });
  };

  render() {
    const { plainText, basicMatrix, encriptedText } = this.state;

    return (
      <div>
        <h3 className="mt-5 mb-3">2. Cu cheia "birodarius"</h3>
        <Row>
          <Col className="left">
            <input
              placeholder="Text de criptat"
              onChange={(event) => {
                this.setState({ plainText: event.target.value });
              }}
            />
            <Button
              className="mt-2"
              onClick={() => {
                this.encrypt(plainText);
              }}
            >
              Criptează
            </Button>
            <div className="encripted-div mt-5">
              <p className="encrypted-text">
                Text criptat:{" "}
                <span>{!isEmpty(encriptedText) ? encriptedText : "-"}</span>
              </p>
            </div>
          </Col>
          <Col>
            <p>Tabelul după care se va face criptarea:</p>
            <Table bordered hover size="lg">
              <thead>
                <tr>
                  <th />
                  <th>1</th>
                  <th>2</th>
                  <th>3</th>
                  <th>4</th>
                  <th>5</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>1</th>
                  <td>{basicMatrix[0][0]}</td>
                  <td>{basicMatrix[0][1]}</td>
                  <td>{basicMatrix[0][2]}</td>
                  <td>{basicMatrix[0][3]}</td>
                  <td>{basicMatrix[0][4]}</td>
                </tr>
                <tr>
                  <th>2</th>
                  <td>{basicMatrix[1][0]}</td>
                  <td>{basicMatrix[1][1]}</td>
                  <td>{basicMatrix[1][2]}</td>
                  <td>{basicMatrix[1][3]}</td>
                  <td>{basicMatrix[1][4]}</td>
                </tr>
                <tr>
                  <th>3</th>
                  <td>{basicMatrix[2][0]}</td>
                  <td>{basicMatrix[2][1]}</td>
                  <td>{basicMatrix[2][2]}</td>
                  <td>{basicMatrix[2][3]}</td>
                  <td>{basicMatrix[2][4]}</td>
                </tr>
                <tr>
                  <th>4</th>
                  <td>{basicMatrix[3][0]}</td>
                  <td>{basicMatrix[3][1]}</td>
                  <td>{basicMatrix[3][2]}</td>
                  <td>{basicMatrix[3][3]}</td>
                  <td>{basicMatrix[3][4]}</td>
                </tr>
                <tr>
                  <th>5</th>
                  <td>{basicMatrix[4][0]}</td>
                  <td>{basicMatrix[4][1]}</td>
                  <td>{basicMatrix[4][2]}</td>
                  <td>{basicMatrix[4][3]}</td>
                  <td>{basicMatrix[4][4]}</td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </div>
    );
  }
}
