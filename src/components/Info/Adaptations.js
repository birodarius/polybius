import React, { Component } from "react";
import { Col, Row, Table } from "react-bootstrap";

export default class Adaptations extends Component {
  render() {
    return (
      <div className="mt-5">
        <h2>Adaptări</h2>
        <p>
          Pătratul Polybius și cifrul Polybius pot fi folosite împreuna cu alte
          metode criptografice cum ar fi Cifrul ADFGVX, Cifrul Homofonic și
          altele.
        </p>

        <h3>Cifrul hibrid între Polybius și Playfair</h3>
        <p>
          Cifrul Playfair este un cifru inventat de Charles Wheatstone și
          promovat de Lyon Playfair. Acesta este bazat pe o matrice de 5 x 5
          similar cu Polybius
        </p>
        <p>
          La început literele sunt aranjate conform cheii, fără repetiția
          literelor, iar restul literelor sunt lăsate la sfărșit. Mesajul este
          împărțit in perechi de litere, iar la sfârsit este pus un "x" daca
          mesajul a fost de lungime impară. De asemenea daca literele din
          pereche sunt la fel, se plasează un "x" între cele două litere și unul
          la sfârșit pentru a compensa adăugarea acestuia. Apoi mesajul este
          criptat cu ajutorul tabelului Playfair și a regulilor.
        </p>
        <p>Regulile sunt acestea:</p>
        <ol>
          <li>
            Dacă literele din pereche apar în același rând din tabel, acestea
            vor fi înlocuite cu literele din dreapta (daca se afla pe coloana
            cea mai din dreapta, se vor lua literele din coloana din prima
            coloana a tabelului).
          </li>
          <li>
            Dacă literele apar pe aceasi coloana in tabel, vor fi înlocuite cu
            literele de pe coloana de dedesubt (coloana de sus, daca acestea
            sunt pe coloana de jos).
          </li>
          <li>
            Dacă literele nu sunt pe aceași coloana și nici pe același rând, vor
            fi înlocuite cu litare de pe același rand, care corespunde cu
            coloana celeilalte litere din pereche.
          </li>
        </ol>
        <p>Tabelul pentru Cifrul playfair cu cheia "playfair"</p>
        <Row>
          <Col>
            <Table striped bordered hover size="sm">
              <tbody>
                <tr>
                  <td>P</td>
                  <td>L</td>
                  <td>A</td>
                  <td>Y</td>
                  <td>F</td>
                </tr>
                <tr>
                  <td>I/J</td>
                  <td>R</td>
                  <td>B</td>
                  <td>C</td>
                  <td>D</td>
                </tr>
                <tr>
                  <td>E</td>
                  <td>G</td>
                  <td>H</td>
                  <td>K</td>
                  <td>M</td>
                </tr>
                <tr>
                  <td>N</td>
                  <td>O</td>
                  <td>Q</td>
                  <td>S</td>
                  <td>T</td>
                </tr>
                <tr>
                  <td>U</td>
                  <td>V</td>
                  <td>W</td>
                  <td>X</td>
                  <td>Z</td>
                </tr>
              </tbody>
            </Table>
          </Col>
          <Col>
            <Table striped bordered hover size="sm">
              <tbody>
                <tr>
                  <td>Mesajul text</td>
                  <td>HELLO WORLD</td>
                </tr>
                <tr>
                  <td>Mesajul Playfair</td>
                  <td>HE LX LO WO RL DX</td>
                </tr>
                <tr>
                  <td>Mesajul criptat cu Cirfrul Playfair</td>
                  <td>KG YV RV VQ GR ZC</td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </div>
    );
  }
}
