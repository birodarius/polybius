import React, { Component } from "react";
import { Col, Row } from "react-bootstrap";
import torches from "../../assets/images/Torches.png";

export default class Applications extends Component {
  render() {
    return (
      <div className="mt-5">
        <h2>Aplicații</h2>
        <h3 className="mt-3">Telegrafie</h3>
        <Row>
          <Col xs={8}>
            <p>
              În screrile lui, Polybius menționeaza nevoia de un mod moi bun de
              smnalizare/comunicare in timpul razboiului, lucru care a condus la
              dezvoltarea "Pătratului Polybius". Înainte de asta erau folosite
              semnalele cu foc, dar erau utile doar pentru mesajele așteptate și
              stabilite înainte, fără a avea un mod de a transmite mesaje despre
              eventimente neașteptate.
            </p>
            <p>
              Pătratul Polybius a fost folosit pentru comunicarea la distanță,
              mai ales pentru semnalele cu foc. Pentru a "trimite" un mesaj, cel
              care dorea sa trimita mesajul ridica doua torțe și aștepta ca
              receptorul să facă același lucru pentru a știi ca sunt gata să
              primească mesajul. Apoi, emițătorul ridica torțele in partea sa
              dreaptă pentru a indica receptorului care tăbliță (sau rând din
              matrice) trebuie sa fie verificată. Apoi, emițătorul va ridica in
              partea sa dreapta torțele indicănd care literă dorește sa o
              folosească. Amândoi, atât emițătorul, cât si receptorul, aveau
              nevoie de aceleasi tăblițe, un telescop (un tub îngust, care
              defapt nu ajuta la vederea la distanță) și torțe.
            </p>
          </Col>
          <Col xs={4}>
            <div className="img-div">
              <img src={torches} alt="" />
            </div>
          </Col>
        </Row>
        <p>
          Același cod a fost folosit și în închisori, prin lovirea tevilor sau a
          pereților. Acest cod poate fi folosit în multe căi, relativ simple(de
          exemplu: cu ajutorul unei lanterne, lămpi, diferite sunete, fum etc)
          și este mult mai usor de învățat decât alte coduri mai sofisticate cum
          ar fi codul Morse, dar este ceva mai putin eficient decat aceste
          coduri mai sofisticate.
        </p>
        <h3 className="mt-3">Steganografie</h3>
        <p>
          Această reprezentare este folosită si in steganofrafie. Numerele de la
          1 la 5 pot fi indicate de noduri pe o sfoară, cusături pe un material,
          litere consecutive înaintea unu spatiu mai mare și multe alte moduri.
        </p>
        <h3 className="mt-3">Criptografie</h3>
        <p>
          Pătratul Polybius este folosit ca și un cifru de baza, numit Cifrul
          lui Polybius. Acest cifru este considerat destul de nesigur conform
          standardelor moderne, pentru ca modelul lui poate fi destul de ușor de
          decriptat.
        </p>
      </div>
    );
  }
}
