import React, { Component } from "react";

export default class PolybiusSquare extends Component {
  render() {
    return (
      <div>
        <h2>Pătratul Polybius</h2>
        <p>
          Pătratul Polybius square este un dispozitiv inventat in grecia antica
          de Cleoxenus si Democleitus și a fost facut cunoscut de istoricul și
          savantul Polybius. Dispozitivul este folosit pentru a fracționa
          caractere așa încât ele pot fi reprezentate de un număr mai mic de
          simboluri, lucru folositor in telegrafie, steganografie și
          criptografie.
        </p>
      </div>
    );
  }
}
