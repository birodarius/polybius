import React, { Component } from "react";
import { Table } from "react-bootstrap";

export default class BasicForm extends Component {
  render() {
    return (
      <div className="mt-5">
        <h2>Forma de bază</h2>
        <p>
          Conform scrierilor lui Polybius, dispozitivul a fost inventat de
          Cleoxenus și Democleitus, dar a fost dezvoltat mai departe de însuși
          Polybius. Dispozitivul împarțea alfabetul în 5 tăblițe cu câte 5
          litire fiecare (mai puțin ultima care avea doar 4). Din nefericire nu
          mai există niciuna din aceste tăblițe antice.
        </p>
        <p>
          Fiecare literă e reprezentată de 2 numere, fiind posibilă
          reprezentarea a 25 de caractere folosind doar 5 simboluri numerice.
        </p>
        <p>Tabelul cu literele grecești:</p>
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th />
              <th>1</th>
              <th>2</th>
              <th>3</th>
              <th>4</th>
              <th>5</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>&Alpha;</td>
              <td>&Beta;</td>
              <td>&Gamma;</td>
              <td>&Delta;</td>
              <td>&Epsilon;</td>
            </tr>
            <tr>
              <td>2</td>
              <td>&Zeta;</td>
              <td>&Eta;</td>
              <td>&Theta;</td>
              <td>&Iota;</td>
              <td>&Kappa;</td>
            </tr>
            <tr>
              <td>3</td>
              <td>&Lambda;</td>
              <td>&Mu;</td>
              <td>&Nu;</td>
              <td>&Xi;</td>
              <td>&Omicron;</td>
            </tr>
            <tr>
              <td>4</td>
              <td>&Pi;</td>
              <td>&Rho;</td>
              <td>&Sigma;</td>
              <td>&Tau;</td>
              <td>&Upsilon;</td>
            </tr>
            <tr>
              <td>5</td>
              <td>&Phi;</td>
              <td>&Chi;</td>
              <td>&Psi;</td>
              <td>&Omega;</td>
            </tr>
          </tbody>
        </Table>
        <p>Dar, cu literele latine, asa arata tabelul:</p>
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th />
              <th>1</th>
              <th>2</th>
              <th>3</th>
              <th>4</th>
              <th>5</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>A</td>
              <td>B</td>
              <td>C</td>
              <td>D</td>
              <td>E</td>
            </tr>
            <tr>
              <td>2</td>
              <td>F</td>
              <td>G</td>
              <td>H</td>
              <td>I/J</td>
              <td>K</td>
            </tr>
            <tr>
              <td>3</td>
              <td>L</td>
              <td>M</td>
              <td>N</td>
              <td>O</td>
              <td>P</td>
            </tr>
            <tr>
              <td>4</td>
              <td>Q</td>
              <td>R</td>
              <td>S</td>
              <td>T</td>
              <td>U</td>
            </tr>
            <tr>
              <td>5</td>
              <td>V</td>
              <td>W</td>
              <td>X</td>
              <td>Y</td>
              <td>Z</td>
            </tr>
          </tbody>
        </Table>
        <p>
          Fiecare literă e reprezetata de coordonatele sale in matrice. De
          exemplu cuvântul "TEST" devine "44 51 34 44". Cele 26 de litere din
          alfabetul Latin nu încap in matricea de 5 x 5, așa că două litere
          trebuie sa fie pe aceași poziție (de obicei, așa cum este mai sus,
          litere I si J vor fi pe aceași poziție, dar o alta practică comuna
          este combinarea literelor C și K pe aceași poziție). De asemenea o
          matrice de 6 x 6 poate fi utilizată pentru a putea folosi atât litere,
          cât și cifre și caractere sapeciale.
        </p>
        <p>
          O matrice de 6 x 6 este des utilizată pentru alfabetul Chirilic (cea
          mai des întâlnită varianta este de 33 de litere, dar unele au pana la
          37) sau cel Japonez.
        </p>
        <p>
          Poate fi folosită o cheie pentru a rearanja literele in matrcie, cu
          litere (eliminănd dublurile) cheii fiind poziționate la începutul
          matricii si cele rămase urmând în ordine alfabetică la sfârșitul
          matricii.
        </p>
        <p>
          De exemplu, folosind cheia "polybius cipher" vom obține următoarea
          matrice.
        </p>
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th />
              <th>1</th>
              <th>2</th>
              <th>3</th>
              <th>4</th>
              <th>5</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>P</td>
              <td>O</td>
              <td>L</td>
              <td>Y</td>
              <td>B</td>
            </tr>
            <tr>
              <td>2</td>
              <td>I/J</td>
              <td>U</td>
              <td>S</td>
              <td>C</td>
              <td>H</td>
            </tr>
            <tr>
              <td>3</td>
              <td>E</td>
              <td>R</td>
              <td>A</td>
              <td>D</td>
              <td>F</td>
            </tr>
            <tr>
              <td>4</td>
              <td>G</td>
              <td>K</td>
              <td>M</td>
              <td>N</td>
              <td>Q</td>
            </tr>
            <tr>
              <td>5</td>
              <td>T</td>
              <td>V</td>
              <td>W</td>
              <td>X</td>
              <td>Z</td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}
